# README.md

## node_modules

| Name | Description |
| ---- | ----------- |
| [jszip](https://stuk.github.io/jszip/) | Utility lib for creating zips |
| [file-saver](https://github.com/eligrey/FileSaver.js#readme) | Lib for saving files, it's not necessary though, since it can easily be done natively. |

## References

- <https://github.com/eligrey/FileSaver.js/wiki/Saving-a-remote-file#using-http-header>
- <https://github.com/jimmywarting/StreamSaver.js>
