import { useCallback } from "react";
import { FILE_1, fetchBlob, downloadBlob, getExampleZip } from "./utils";

function App() {
  const downloadSingle = useCallback(async () => {
    try {
      const blob = await fetchBlob(FILE_1);
      downloadBlob(blob, "todo-1.json");
    } catch {
      console.log("Something happened");
    }
  }, []);

  const downloadMultiple = useCallback(async () => {
    try {
      const generatedZip = await getExampleZip();
      downloadBlob(generatedZip, "example.zip");
    } catch {
      console.log("Something happened.");
    }
  }, []);

  return (
    <div className='App'>
      <h1>Feature A</h1>
      <button onClick={() => window.open(FILE_1)}>Open in new tab</button>
      <button onClick={() => downloadSingle()}>Download Single</button>
      <button onClick={() => downloadMultiple()}>Download ZIP</button>
    </div>
  );
}

export default App;
