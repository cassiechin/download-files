import JSZip from "jszip";

export const FILE_1 = "https://jsonplaceholder.typicode.com/todos/1";
export const FILE_2 = "https://jsonplaceholder.typicode.com/todos/2";

export const fetchBlob = async (file: string) => {
  const resp = await fetch(file);
  const blob = await resp.blob();
  return blob;
};

// Could alternatively use the saveAs() method from the 'file-saver' package
export const downloadBlob = async (blob: Blob, filename: string) => {
  const url = window.URL.createObjectURL(blob);

  const anchor = document.createElement("a");
  anchor.style.display = "none";
  anchor.href = url;
  anchor.download = filename;
  document.body.appendChild(anchor);
  anchor.click();

  window.URL.revokeObjectURL(url);
};

/**
 * Creates a ZIP file with the following structure
 * - todo-1.json
 * - inner/todo-2.json
 */
export const getExampleZip = async () => {
  const blob1 = await fetchBlob(FILE_1);
  const blob2 = await fetchBlob(FILE_2);

  const zip = new JSZip();
  zip.file("todo-1.json", blob1);
  const innerFolder = zip.folder("inner");
  if (innerFolder) {
    innerFolder.file("todo-2.json", blob2);
  } else {
    console.log("Inner folder was not created");
  }
  return await zip.generateAsync({ type: "blob" });
};
